# OpenML dataset: Covid-19--historical-data

https://www.openml.org/d/43733

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Includes data on confirmed cases, deaths, hospitalizations, and testing, as well as other variables of potential interest.
Content
As of 26 January 2021, the columns are: isocode, continent, location, date, totalcases, newcases, newcasessmoothed, totaldeaths, newdeaths, newdeathssmoothed, totalcasespermillion, newcasespermillion, newcasessmoothedpermillion, totaldeathspermillion, newdeathspermillion, newdeathssmoothedpermillion, reproductionrate, icupatients, icupatientspermillion, hosppatients, hosppatientspermillion, weeklyicuadmissions, weeklyicuadmissionspermillion, weeklyhospadmissions, weeklyhospadmissionspermillion, totaltests, newtests, totaltestsperthousand, newtestsperthousand, newtestssmoothed, newtestssmoothedperthousand, positiverate, testspercase, testsunits, totalvaccinations, peoplevaccinated, peoplefullyvaccinated, newvaccinations, newvaccinationssmoothed, totalvaccinationsperhundred, peoplevaccinatedperhundred, peoplefullyvaccinatedperhundred, newvaccinationssmoothedpermillion, stringencyindex, population, populationdensity, medianage, aged65older, aged70older, gdppercapita, extremepoverty, cardiovascdeathrate, diabetesprevalence, femalesmokers, malesmokers, handwashingfacilities, hospitalbedsperthousand, lifeexpectancy, humandevelopment_index
Acknowledgements
https://covid.ourworldindata.org/data/owid-covid-data.csv
Inspiration
Fight the deadly virus

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43733) of an [OpenML dataset](https://www.openml.org/d/43733). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43733/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43733/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43733/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

